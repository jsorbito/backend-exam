<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\GuestComment as GuestComment;

class CommentGuestController extends Controller
{
  public function getComment(Request $param){
  	$data = ["title" => strip_tags($param->input('title')),
  			 "body" => strip_tags($param->input('body')),
  			 "id" => strip_tags($param->input('id'))
  			];
  	$response = ["data" => []];
  	if(!is_numeric($data["id"])){
  		$response["data"] = ["Please login first"];

  	}else{
	   	$insert = new GuestComment;
		$insert->title    = $data["title"];
	    $insert->body     = $data["body"];
	    $insert->commentable_id = $data["id"];
	    $insert->creator_id     = $data["id"];;
		$insert->save();
		$insertedId = $insert->id;
		$newData = GuestComment::where('id',$insertedId)->first();
		$response["data"] = $newData;
	}
    return response()->json($response);
 }
 public function postComment(Request $param){
  	$data = ["title" => strip_tags($param->input('title')),
  			 "body" => strip_tags($param->input('body')),
  			 "id" => strip_tags($param->input('id'))
  			];
  	$response = ["data" => []];
  	if(!is_numeric($data["id"])){
  		$response["data"] = ["Please login first"];

  	}else{
	   	$insert = new GuestComment;
		$insert->title    = $data["title"];
	    $insert->body     = $data["body"];
	    $insert->commentable_id = $data["id"];
	    $insert->creator_id     = $data["id"];;
		$insert->save();
		$insertedId = $insert->id;
		$newData = GuestComment::where('id',$insertedId)->first();
		$response["data"] = $newData;
	}
    return response()->json($response);
 }

 public function patchComment(Request $param){
  	$data = ["comment_id" => strip_tags($param->input('comment_id')),
  			 "body" => strip_tags($param->input('body')),
  			 "id" => strip_tags($param->input('id'))
  			];
  	$response = ["data" => []];
  	$hasError = false;
  	if(!is_numeric($data["id"])){
  		array_push($response["data"],"Please login first");
  		$hasError = true;
  	}
  	if(!is_numeric($data["comment_id"])){
  		array_push($response["data"],"Invalid comment id");
  		$hasError = true;
  	}

  	if(!$hasError){
	    $alter = GuestComment::where('id',$data['comment_id'])->first();
	    if(is_null($alter)){
	    	array_push($response["data"],"Invalid comment id");
	    }else{
	   	    $alter->body = $data["body"];
			$alter->save();
			$response["data"] = $alter;
		}
	}
    return response()->json($response);
 }
 public function deleteComment(Request $param){
  	$data = ["comment_id" => strip_tags($param->input('comment_id')),
  			 "id" => strip_tags($param->input('id'))
  			];
  	$response = ["data" => []];
  	$hasError = false;
  	if(!is_numeric($data["id"])){
  		array_push($response["data"],"Please login first");
  		$hasError = true;
  	}
  	if(!is_numeric($data["comment_id"])){
  		array_push($response["data"],"Invalid comment id");
  		$hasError = true;
  	}

  	if(!$hasError){
	    $alter = GuestComment::where('id',$data['comment_id'])->first();
	    if(is_null($alter)){
	    	array_push($response["data"],"Invalid comment id");
	    }else{
			$alter->delete();
			$response["data"] = ["status" => "record deleted successfully"];
		}
	}
    return response()->json($response);
 }

}
