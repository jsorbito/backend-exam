<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Register as Register;

class RegisterController extends Controller
{

 public function postLogin(Request $param){
  	$response = [];
  	$errors = ["required"=>[],"credentials"=>[]];
  	$data = ["pass" => strip_tags($param->input('pass')),
  			 "email" => strip_tags($param->input('email'))
  			];
  	$hasError = false;
  	
  	foreach($data as $key => $per){ 
  		if(strlen(trim($per)) <= 0){
  			array_push($errors['required'],$key);
  			$hasError = true;
  		}
  	}//check if has value

  	$email = Register::where('email',$data["email"])->first();
  	if(isset($email->email)){
  		if($email->pass !== $data["pass"]){
  			array_push($errors['credentials'], "Invalid");
  			$hasError = true;
  		}
  	}else{
  		   array_push($errors['credentials'], "Invalid");
  			$hasError = true;
  	}//email check
  	
  	if($hasError){
  		$response["message"] = "The given data is invalid";
  		$response["errors"] = $errors;
  	}

  	if(sizeof($response) <= 0){

		$response = ["token" => base64_encode(random_bytes(64)),
					       "token_type" => "bearer",
					       "expires_at" => "2020-01-23 01:55:46",
                 "id" => $email->id
					];
  	}

    return response()->json($response);
   }

  public function postInsert(Request $param){
  	$response = [];
  	$errors = ["required"=>[],"password"=>[],"email"=>[]];
  	$data = ["name" => strip_tags($param->input('name')),
  			 "pass" => strip_tags($param->input('pass')),
  			 "confirmation" => strip_tags($param->input('confirmation')),
  			 "email" => strip_tags($param->input('email'))
  			];
  	$hasError = false;
  	
  	foreach($data as $key => $per){ 
  		if(strlen(trim($per)) <= 0){
  			array_push($errors['required'],$key);
  			$hasError = true;
  		}
  	}//check if has value

	if($data["pass"] !== $data["confirmation"]){
  		array_push($errors['password'],"Dont match");
  		$hasError = true;
  	}//check password match

  	$chechMail = Register::where('email',$data["email"])->get();

  	if(sizeof($chechMail) > 0){
  		array_push($errors['email'], "Email is taken");
  		$hasError = true;
  	}//email check

  	if($hasError){
  		$response["message"] = "The given data is invalid";
  		$response["errors"] = $errors;
  	}

  	if(sizeof($response) <= 0){
  		$insert = new Register;
  		$insert->name    = $data["name"];
      $insert->pass    = $data["pass"];
      $insert->confirm = $data["confirmation"];
      $insert->email   = $data["email"];
	    $insert->save();

		$insertedId = $insert->id; //new data
		$newData = Register::where('id',$insertedId)->first(['name','email','updated_at','created_at','id']);
		$response = $newData;
  	}

    return response()->json($response);
   }
   public function postLogout(Request $param){
    return response()->json(["status"=>"log out sucessfully"]);
   }

}
