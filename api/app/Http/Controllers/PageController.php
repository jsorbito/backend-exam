<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Page as Page;

class PageController extends Controller
{

  public function getPage(Request $param){

  	$data = ["title" => "new title",
  			 "slug" => "new slug",
  			 "content" => "new content",
  			 "page" => strip_tags($param->input('page')),
  			 "id" => strip_tags($param->input('id'))
  			];
 	$hasError = false;
  	$response = ["data" => [],"links"=>[],"meta"=>[]];
  	if(!is_numeric($data["id"])){
  		array_push($response["data"],"Please login first");
  		$hasError = true;
  	}
  	if(!is_numeric($data["page"])){
  		array_push($response["data"],"Invalid page");
  		$hasError = true;
  	}

  	if(!$hasError){
	   	$insert = new Page;
		$insert->title    = $data["title"];
	    $insert->content  = $data["content"];
	    $insert->slug     = $data["slug"];
	    $insert->user_id  = $data["id"];;
		$insert->save();
		$insertedId = $insert->id;
		$newData = Page::where('id',$insertedId)->first();
		$response["data"] = $newData;
		$response["links"] = ["first" => url()->full(),
							  "last"  => url()->full(),
							  "prev"  => null,
							  "next"  => null
							 ];
		$response["meta"] = ["current_page" => $data["page"],
							  "from"  =>  $data["page"],
							  "last_page"  => $data["page"],
							  "path"  => url()->current(),
							  "per_page"  => 15,
							  "to"  => $data["page"],
							  "total"  => $data["page"],
							 ];
	}
    return response()->json($response);
 }
 public function getTitle(Request $param){

  	$data = ["title" => "new title",
  			 "slug" => "new slug",
  			 "content" => "new content",
  			 "id" => strip_tags($param->input('id'))
  			];
 	$hasError = false;
  	$response = ["data" => []];
  	if(!is_numeric($data["id"])){
  		array_push($response["data"],"Please login first");
  		$hasError = true;
  	}
  	if(!$hasError){
	   	$insert = new Page;
		$insert->title    = $data["title"];
	    $insert->content  = $data["content"];
	    $insert->slug     = $data["slug"];
	    $insert->user_id  = $data["id"];;
		$insert->save();
		$insertedId = $insert->id;
		$newData = Page::where('id',$insertedId)->first();
		$response["data"] = $newData;
	}
    return response()->json($response);
 }

}

