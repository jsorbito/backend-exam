<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/register/test','RegisterController@getTest')->name('getTest');
Route::get('/api/posts/{guest}/comments','CommentGuestController@getComment')->name('getComment');
Route::get('/api/posts/page','PageController@getPage')->name('getPage');
Route::get('/api/posts/title','PageController@getTitle')->name('getTitle');

Route::post('/api/register','RegisterController@postInsert')->name('postRegister');
Route::post('/api/login','RegisterController@postLogin')->name('postLogin');
Route::post('/api/logout','RegisterController@postLogout')->name('postLogout');
Route::post('/api/posts/{post}/comments','CommentGuestController@postComment')->name('postComment');

Route::patch('/api/posts/{post}/comments/{id}','CommentGuestController@patchComment')->name('patchComment');

Route::delete('/api/posts/{post}/comments/{id}','CommentGuestController@deleteComment')->name('deleteComment');
