-create database named "exam"
- run migration "php artisan migrate"
- run server "php artisan serve" will open localhost:8000
- run Fe server "node server.js" will open localhost:3000
 -sequence
  - register
  - login
     -comments
     -page
     -title
  -logout
    - re login



To register(GUEST:POST):
  * goto url 'localhost:3000/api/register' and fill in params(name,pass,confirmation,email);

  Ex.
  "localhost:3000/api/register?name=J Orland Sorbito&pass=faith&confirmation=faith&email=jsorbito.bacolod@gmail.com"

To login(GUEST:POST):
  * goto url 'localhost:3000/api/login' and fill in params(pass,email);

  Ex.
  "localhost:3000/api/login?pass=faith&email=jsorbito.bacolod@gmail.com"

To Comments(GUEST:GET):
  * should login first
  * goto url http://localhost:3000/api/posts/guest/comments and fill in params(title,body);

  Ex.
  "http://localhost:3000/api/posts/guest/comments?title=faith&body=the wheel will turn"

To Page(GUEST:GET):
  * should login first
  * goto url http://localhost:3000/api/posts/page and fill in params(page);

  Ex.
  "http://localhost:8000/api/posts/page?page=2"

To Title(GUEST:GET):
  * should login first
  * goto url http://localhost:3000/api/posts/new-tile and fill in params(page);
Ex.
  "http://localhost:3000/api/posts/new-title"
  

**--------------------------------------------------------------**



To Comments(AUTH:POST):
  * should login first
  * goto url http://localhost:3000/api/posts/auth/comments and fill in params(title,body);

  Ex.
  "http://localhost:3000/api/posts/auth/comments?title=faith&body=the wheel will turn"

To CommentsAlter(AUTH:PATCH):
  * should login first
  * goto url http://localhost:3000/api/posts/auth/comments and fill in params(comment_id,body);

  Ex.
  "http://localhost:3000/api/posts/auth/comments/id?comment_id=1&body=I have faith"

To CommentsDelete(AUTH:DELETE):
  * should login first
  * goto url http://localhost:3000/api/posts/auth/comments/delete and fill in params(comment_id);

  Ex.
  "http://localhost:3000/api/posts/auth/comments/delete?comment_id=3"

To Logout(AUTH:POST):
  * goto url http://localhost:3000/api/logout

  Ex.
  "http://localhost:3000/api/logout"