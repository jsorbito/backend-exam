let express = require("express");
let api = express();
let fetch = require('node-fetch');
let parser = require("cookie-parser");
api.use(parser());


api.get('/api/posts/new-title',(req,res) =>{
const param = req.query;
	fetch(`http://localhost:8000/api/posts/title?id=${req.cookies.userId}`)
	.then(res => res.json())
	.then(json => res.send(json))
	.catch(err=>console.error(err));
});

api.get('/api/posts/page',(req,res) =>{
const param = req.query;
	fetch(`http://localhost:8000/api/posts/page?page=${param.page}&id=${req.cookies.userId}`)
	.then(res => res.json())
	.then(json => res.send(json))
	.catch(err=>console.error(err));
});

api.get('/api/posts/guest/comments',(req,res) =>{
const param = req.query;
	fetch(`http://localhost:8000/api/posts/guest/comments?title=${param.title}&body=${param.body}&id=${req.cookies.userId}`)
	.then(res => res.json())
	.then(json => res.send(json))
	.catch(err=>console.error(err));
});


api.get('/api/register',(req,res) =>{

const body = req.query;
	fetch('http://localhost:8000/api/register',{
	        method: 'post',
	        body:    JSON.stringify(body),
	        headers: {'Content-Type': 'application/json'}
	    })
	    .then(res => res.json()) 
		.then(json => res.send(json)).catch(err=>console.error(err));
});

api.get('/api/login',(req,response) =>{
const body = req.query;
	fetch('http://localhost:8000/api/login',{
	        method: 'post',
	        body:    JSON.stringify(body),
	        headers: {'Content-Type': 'application/json'}
	    })
		.then(res => res.json()) 
		.then(json => { response.cookie('userId',json.id); response.send(json)}).catch(err=>console.error(err));
});

api.get('/api/posts/auth/comments',(req,response) =>{
const body = req.query;
      body["id"] = req.cookies.userId;
	fetch('http://localhost:8000/api/posts/user-post/comments',{
	        method: 'post',
	        body:    JSON.stringify(body),
	        headers: {'Content-Type': 'application/json'}
	    })
		.then(res => res.json()) 
		.then(json => response.send(json)).catch(err=>console.error(err));
});

api.get('/api/posts/auth/comments/id',(req,response) =>{
const body = req.query;
      body["id"] = req.cookies.userId;
	fetch('http://localhost:8000/api/posts/user-post/comments/id',{
	        method: 'patch',
	        body:    JSON.stringify(body),
	        headers: {'Content-Type': 'application/json'}
	    })
		.then(res => res.json()) 
		.then(json => response.send(json)).catch(err=>console.error(err));
});

api.get('/api/posts/auth/comments/delete',(req,response) =>{
const body = req.query;
      body["id"] = req.cookies.userId;
	fetch('http://localhost:8000/api/posts/user-post/comments/id',{
	        method: 'delete',
	        body:    JSON.stringify(body),
	        headers: {'Content-Type': 'application/json'}
	    })
		.then(res => res.json()) 
		.then(json => response.send(json)).catch(err=>console.error(err));
});

api.get('/api/logout',(req,response) =>{
const body = req.query;
	fetch('http://localhost:8000/api/logout',{
	        method: 'post',
	        body:    JSON.stringify(body),
	        headers: {'Content-Type': 'application/json'}
	    })
		.then(res => res.json()) 
		.then(json => { response.clearCookie('userId'); response.send(json)}).catch(err=>console.error(err));
});

api.listen(3000);